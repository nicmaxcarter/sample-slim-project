<?php

declare(strict_types=1);

use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use App\Middleware\AuthMiddlware;

$container = $app->getContainer();

return function (App $app) {
    $app->get('/', 'App\Controller\HomeController:index')->setName('home')->add(new AuthMiddlware());
    $app->map(['GET', 'POST'], '/login', 'App\Controller\AuthController:login')->setName('login');

    $app->group('/member', function (Group $group) {
        $group->get('/logout', 'App\Controller\AuthController:logout')
              ->setName('logout');
        $group->get('/employees', 'App\Controller\EmployeeController:index')
              ->setName('employees.home');
        $group->get('/employees/{id}', 'App\Controller\EmployeeController:showEmployee')
              ->setName('employees.ind');
        $group->get('/customers', 'App\Controller\CustomerController:index')
              ->setName('customers.home');
        $group->get('/customers/{id}', 'App\Controller\CustomerController:showCustomer')
              ->setName('customers.ind');
        $group->get('/invoices', 'App\Controller\InvoicesController:index')
              ->setName('invoices.home');
        $group->get('/invoices/{id}', 'App\Controller\InvoicesController:showInvoice')
              ->setName('invoices.ind');
    })->add(new AuthMiddlware());
};
