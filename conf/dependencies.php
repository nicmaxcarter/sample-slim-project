<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use Doctrine\DBAL\DriverManager;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        'logger' => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler(
                $loggerSettings['path'],
                $loggerSettings['level']
            );

            $logger->pushHandler($handler);

            return $logger;
        },
        'upload_directory' => function (ContainerInterface $container) {
            return __DIR__ . '/../public/files/tmp';
        },

        'db' => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            return DriverManager::getConnection(
                $settings['doctrine']['connection']
            );
        },
        'session' => function (ContainerInterface $container) {
            return new \App\Middleware\SessionMiddleware();
        },
        'flash' => function (ContainerInterface $container) {
            $session = $container->get('session');
            return new \Slim\Flash\Messages($session);
        },
        'view' => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            return Twig::create(
                $settings['view']['template_path'],
                $settings['view']['twig']
            );
        },
    ]);
};
