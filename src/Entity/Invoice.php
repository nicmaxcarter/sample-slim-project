<?php

namespace App\Entity;

use Doctrine\DBAL\Connection as DB;

class Invoice
{
    protected DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return array<mixed>
     */
    public function getAllInvoices(): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Invoice');

        return $query->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param int $x
     * @return array<mixed>
     */
    public function getInvoicesPartial(int $x): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Invoice')
            ->setMaxResults($x)
            ->orderBy('CustomerId', 'DESC');

        $result = $query->executeQuery()->fetchAllAssociative();

        return $result;
    }

    /**
     * @param int $id
     * @return array<mixed>
     */
    public function getInvoice(int $id): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Invoice')
            ->where('InvoiceId = (:id)')
            ->setParameter('id', $id);

        $result = $query->executeQuery()->fetchAssociative();

        if ($result === false) {
            return [];
        }

        return $result;
    }
}
