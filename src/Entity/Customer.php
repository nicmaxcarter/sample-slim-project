<?php

namespace App\Entity;

use Doctrine\DBAL\Connection as DB;

class Customer
{

    protected DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return array<mixed>
     */
    public function getAllCustomers(): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Customer');

        return $query->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param int $x
     * @return array<mixed>
     */
    public function getCustomersPartial(int $x): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Customer')
            ->setMaxResults($x);

        $result = $query->executeQuery()->fetchAllAssociative();

        foreach($result as $key=>$row) {
            if(is_null($row['Company'])){
                $result[$key]['Company'] = "n/a";
            }
            if(is_null($row['State'])){
                $result[$key]['State'] = "n/a";
            }
        }

        return $result;
    }

    /**
     * @param int $id
     * @return array<mixed>
     * return customer data by customer ID
     */
    public function getCustomer(int $id): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Customer')
            ->where('CustomerId = (:id)')
            ->setParameter('id', $id);

        $result = $query->executeQuery()->fetchAssociative();

        if ($result === false) {
            return [];
        }

        return $result;
    }

}
