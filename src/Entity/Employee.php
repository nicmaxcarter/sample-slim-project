<?php

namespace App\Entity;

use Doctrine\DBAL\Connection as DB;

class Employee
{
    protected DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return array<mixed>
     */
    public function getAllEmployees(): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Employee');

        return $query->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param int $x
     * @return array<mixed>
     * return X number of employees
     */
    public function getEmployeesPartial(int $x): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Employee')
            ->setMaxResults($x);

        return $query->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param int $id
     * @return array<mixed>
     * return employee data by employee ID
     */
    public function getEmployee(int $id): array
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('Employee')
            ->where('EmployeeId = (:id)')
            ->setParameter('id', $id);

        $result = $query->executeQuery()->fetchAssociative();

        if ($result === false) {
            return [];
        }

        return $result;
    }
}
