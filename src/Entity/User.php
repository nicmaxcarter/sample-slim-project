<?php

namespace App\Entity;

use Doctrine\DBAL\Connection as DB;

class User
{
    protected DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $username
     * @return array<mixed>|false
     */
    public function getUserByName(string $username): array|false
    {
        $query = $this->db->createQueryBuilder();

        $query->select('*')
            ->from('user')
            ->where('username = (:username)')
            ->setParameter('username', $username);

        return $query->executeQuery()->fetchAssociative();
    }
}
