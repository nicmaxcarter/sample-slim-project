<?php

namespace App\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;
use Doctrine\DBAL\Connection as DB;
use Monolog\Logger;
use Slim\Flash\Messages;

abstract class BaseController
{
    protected Twig $view;
    protected Logger $logger;
    protected Messages $flash;
    protected DB $db;

    public function __construct(ContainerInterface $container)
    {
        $this->view = $container->get('view');
        $this->logger = $container->get('logger');
        $this->flash = $container->get('flash');
        $this->db = $container->get('db');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param string $template
     * @param array<mixed> $params
     * @return Response
     */
    protected function render(
        Request $request,
        Response $response,
        string $template,
        array $params = []
    ): Response {

        $params['flash'] = $this->flash->getMessage('info');

        $params['uinfo'] = $request->getAttribute('uinfo');

        return $this->view->render($response, $template, $params);
    }
}
