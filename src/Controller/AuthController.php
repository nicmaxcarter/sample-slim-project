<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Entity\User;

final class AuthController extends BaseController
{
    /**
     * @param string $uname
     * @param string $pswd
     * @return array<mixed>|null
     */
    private function auth(string $uname, string $pswd): ?array
    {
        $user = new User($this->db);

        $uinfo = $user->getUserByName($uname);

        if ($uinfo === false) {
            return null;
        }

        if (!password_verify($pswd, $uinfo['password'])) {
            return null;
        }

        return $uinfo;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function login(
        Request $request,
        Response $response,
        array $args = []
    ): Response {
        if ($request->getMethod() == 'POST') {
            $data = (array)$request->getParsedBody();

            if (empty($data["uname"]) || empty($data["pswd"])) {
                $this->flash->addMessage('info', 'Empty value in login/password');
                return $response->withStatus(302)->withHeader('Location', '/login');
            }

            // Check the user username / pass
            $uinfo = $this->auth($data["uname"], $data['pswd']);
            if ($uinfo == null) {
                $this->flash->addMessage('info', 'Invalid login/password');
                return $response->withStatus(302)->withHeader('Location', '/login');
            }


            $session = $request->getAttribute('session');
            $session['logged'] = true;
            $session['uinfo'] = [
                'id' => $uinfo['id'],
                'firstname' => $uinfo['first_name'],
                'lastname' => $uinfo['last_name'],
                'email' => $uinfo['email']
            ];

            $this->flash->addMessage('info', 'Logged');

            return $response->withStatus(302)->withHeader('Location', '/');
        }
        return $this->view->render($response, 'login.twig', [
            'loginPage' => true,
            'flash' => $this->flash->getMessage('info'),
            'uinfo' => $request->getAttribute('uinfo')
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function logout(
        Request $request,
        Response $response,
        array $args = []
    ): Response {
        $session = $request->getAttribute('session');

        $session['logged'] = false;

        unset($session['uinfo']);

        return $response->withStatus(302)->withHeader('Location', '/');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isLoggedIn(Request $request): bool
    {
        return $request->getAttribute('session')['logged'];
    }
}
