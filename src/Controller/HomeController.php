<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Entity\Employee as Employee;
use App\Entity\Customer as Customer;

final class HomeController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function index(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("Home page action dispatched");

        // get list of 4 employees
        $employee = new Employee($this->db);
        $employees = $employee->getEmployeesPartial(5);

        // get list of 4 customers
        $customer = new Customer($this->db);
        $customers = $customer->getCustomersPartial(5);

        return $this->render($request, $response, 'dash.html', [
            "employees" =>  $employees,
            "customers" =>  $customers
        ]);
    }
}
