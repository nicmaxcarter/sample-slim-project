<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Entity\Customer as Customer;

final class CustomerController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function index(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("Customers dispatched");

        $customer = new Customer($this->db);

        $customers = $customer->getCustomersPartial(12);

        return $this->render($request, $response, 'customer/customers.html', [
            "customers" =>  $customers
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function showCustomer(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("View individual customer: " . $args['id']);

        $customer = new Customer($this->db);

        $customerProfile = $customer->getCustomer(intval($args['id']));

        return $this->render($request, $response, 'entry.html', [
            "title" =>  "Customer",
            "entry" =>  $customerProfile
        ]);
    }
}
