<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Entity\Invoice as Invoice;

final class InvoicesController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function index(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("Invoices dispatched");

        $invoice = new Invoice($this->db);

        $invoices = $invoice->getInvoicesPartial(12);

        return $this->render($request, $response, 'invoice/invoices.html', [
            "invoices" =>  $invoices
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function showInvoice(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("View individual invoice: " . $args['id']);

        $invoice = new Invoice($this->db);

        $invoiceProfile = $invoice->getInvoice(intval($args['id']));

        return $this->render($request, $response, 'entry.html', [
            "title" =>  "Invoice",
            "entry" =>  $invoiceProfile
        ]);
    }
}
