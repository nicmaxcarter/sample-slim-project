<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Entity\Employee as Employee;

final class EmployeeController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function index(
        Request $request,
        Response $response,
        array $args = []
    ): Response {

        $this->logger->info("Employees dispatched");

        $employee = new Employee($this->db);

        $employees = $employee->getEmployeesPartial(12);

        return $this->render($request, $response, 'employee/employees.html', [
            "employees" =>  $employees
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<mixed> $args
     * @return Response
     */
    public function showEmployee(
        Request $request,
        Response $response,
        array $args = []
    ): Response {
        $this->logger->info("View individual employee: " . $args['id']);

        $employee = new Employee($this->db);

        $employeeProfile = $employee->getEmployee(intval($args['id']));

        return $this->render($request, $response, 'entry.html', [
            "title" =>  "Employee",
            "entry" =>  $employeeProfile
        ]);
    }
}
